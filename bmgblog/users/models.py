from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Profile (models.Model):
    # 建立一個profile的model，內含user跟image

    user = models.OneToOneField(User, on_delete=models.CASCADE) # delete user delete profile，但反過來不會
    image = models.ImageField(default='default.jpg', upload_to='profile_pics') 
    # pic會上傳到profile_pics資料夾，這個資料夾會跟manage.py在同一個資料夾中
    # 但可以改變這個位置

    def __str__(self):
        return f'{self.user.username} Profile' #如果沒有設定這個的話，只會print Profile這個物件，因此要更明確的告訴程式要印出內容
