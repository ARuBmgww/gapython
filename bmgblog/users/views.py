from django.shortcuts import render, redirect
# from django.contrib.auth.forms import UserCreationForm
# django 已經有內建的密碼驗證套件
from django.contrib import messages
from .form import UserRegisterForm, UserUpdateForm, profileUpdateForm
# 這是我們自己創立的套件，接著就可以取代原本 UserCreationForm
# Update form是給使用者修改個人資料用的

from django.contrib.auth.decorators import login_required
#這個是要防止沒有登入時去看profile網站


# Create your views here.
def register(request):

    if request.method == 'POST': # 如果request method是post
        form = UserRegisterForm(request.POST)

        if form.is_valid(): #確認輸入的資料都符合內建的要求，正確

            form.save() # 一切都正確，就存到後台

            username = form.cleaned_data.get('username') # 如果有效就取出使用者名稱。cleaned_data 是把資料轉成python型式
            messages.success(request, f'Account create for {username}!') # f 就是flash message，只顯示一次的訊息
            return redirect('blog-home')   # 成功建立後就重新轉移到 home

    else:
        form = UserRegisterForm() #沒有post就單純建立一個空白的表單
    
    return render(request,'users/signup.htm',{'form':form}) #同樣的要用dict的型式把資料傳到template

@login_required #在login的前提下進行下面的function
def profile(request):
    if request.method == 'POST':
        u_form = UserUpdateForm(request.POST,instance=request.user)
        p_form = profileUpdateForm(request.POST,request.FILES,instance=request.user.profile)

        if u_form.is_valid() and p_form.is_valid():
            u_form.save()
            p_form.save()
            messages.success(request, f'Your account has been updates!')
            return redirect('profile')
    else:
        u_form = UserUpdateForm(instance=request.user)
        p_form = profileUpdateForm(instance=request.user.profile)


    context = {
        'u_form':u_form,
        'p_form':p_form,
    }
    return render(request, 'users/profile.htm' , context)