from django.db.models.signals import post_save
# 當post_save時就發射一個訊號出來，在本範例中就是user建立時

from django.contrib.auth.models import User
# User model在這邊被稱為sender，傳遞訊號

from django.dispatch import receiver
# 這邊是要有人去接受訊號 receiver

from .models import Profile
# 因為要建立profil，所以自然要import進來


@receiver(post_save,sender=User) # 當user saved時，傳遞一個send = User的訊號到 receiver，而這個receiver正好是下面這個create_profile的function
def create_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance) #把上面的東西接收進來，當created時就立刻建立一個user的profile

# 建立後還要save
@receiver(post_save,sender=User) 
def save_profile(sender, instance, **kwargs):
    instance.profile.save()
