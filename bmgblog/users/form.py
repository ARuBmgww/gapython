from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import Profile


class UserRegisterForm(UserCreationForm): #先繼承
    email = forms.EmailField()  #自行創立新的項目，並且設定格式

    class Meta:
        model = User 
        # 設定這個 客制化的表單所需要的model。選user是因為這是用來新增user的
        
        fields = ['username', 'email','password1', 'password2' ] 
        # 這邊表示要列出的項目。password1使用者第一次輸入的。password2使用者驗證的
        # 會用一個nested的class Meta是因為這個只有在

class UserUpdateForm(forms.ModelForm):
    email = forms.EmailField()  #自行創立新的項目，並且設定格式

    class Meta:
        model = User 
        fields = ['username', 'email', ] 
    
class profileUpdateForm(forms.ModelForm):
    class Meta:
        model = Profile 
        fields = ['image'] 
