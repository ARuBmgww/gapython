# app裏面也要自己建立一個urls.py
# 讓app中的東西可以藉由連結傳遞給使用者

from django.contrib import admin
from django.urls import path
from .views import PostListView, PostDetailView, PostCreateView, PostUpdateView, PostDeleteView
from . import views
#從這個資料夾中import views。 . 代表所在為同一個資料夾

urlpatterns = [
    path('',PostListView.as_view(),name='blog-home'),
    # 不能只是傳model過來還得確實的轉成真正要用的.as_view()

    path('post/<int:pk>', PostDetailView.as_view(), name='post-detail'),
    path('post/new/', PostCreateView.as_view(), name='post-create'),
    path('post/<int:pk>/update', PostUpdateView.as_view(), name='post-update'),
    path('post/<int:pk>/delete', PostDeleteView.as_view(), name='post-delete'),

    #path('',views.home_test, name='home_test'),
    #path('home/',views.home, name='blog-home'),
    path('about/',views.about, name='blog-about'),
    path('signup/',views.signup, name='blog-sign_up'),
    path('signup_2/',views.signup_2, name='blog-sign_up2'),
    path('thank/',views.thank, name='blog-thank_for_post'),

]

# urlpatterns: 把使用者輸入的網址對應到特定的位址
# path('',views.home, name='blog-home')
# 因為是一進來就看到的homepage，所以空白不用連結到特定的名稱
# views.home 代表去抓views中 def home的功能
# 最後是名稱，也就是這個頁面的名稱。
