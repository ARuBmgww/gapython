from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User #這個是用來紀錄User或者author的
from django.urls import reverse

# Create your models here.

# 因為是blog所以先簡單建立一個貼文的db
class Post(models.Model):
    # 從django內建的格式來繼承

    title = models.CharField(max_length=100)
    # 用來放標題的，所以用charfiled，最大100字

    context = models.TextField()
    # 放貼文內容的，一行一行的內容所以用 textfiled()，代表長度不限制

    date_posted = models.DateTimeField(default=timezone.now)
    # 紀錄時間，這邊很多選項
    # DateTimeField(auto_now = True) -> 每次更新就紀錄當下時間 (比較適合用在紀錄last modify time)
    # DateTimeField(auto_now_add = True) -> 紀錄貼文建立的時間，但無法隨著更新而紀錄時間
    # 所以用 from django.utils import timezone
    #           DateTimeField(defaul= timezone.now)
    # 這邊不用 timezone.now()，因為不是要執行這個方法，而是看他的值放進去default中

    author = models.ForeignKey(User, on_delete=models.CASCADE)
    # author 是文章作者，一篇文章只會有一歌人
    # User 是使用者，一個使用者能有多篇文章
    # 而且兩者是有關係的。用ForenignKey
    # author = models.ForeignKey(User). author只會對應到獨一的User
    # on_delete=  決定如果user刪除後要做什麼動作
    # on_delete=models.CASECADE -> User 如果刪文了，也把author刪掉
    # 但這是on way的，不會因為刪除auth反而把User也刪除

    def __str__(self):
        return self.title
    # 每當資料建立後，回傳title出來通常作為確認用

    def get_absolute_url(self):
        return reverse("post-detail", kwargs={"pk": self.pk})
    # 這邊是讓model能夠取得前往'oist-detail'的網址
    # 然後交給views帶過去
        


