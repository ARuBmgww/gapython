from django.shortcuts import render
from .models import Post
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.views.generic import (
    ListView, 
    DetailView,
    CreateView,
    UpdateView,
    DeleteView,
)
# 這個讓我們可以使用templates找到檔案

from django.http import HttpResponse

# Create your views here.

posts = [
    {
        'author':'CoreMs',
        'title':'Blog Post 1',
        'context':'First post content',
        'date_posted':'August 27, 2019',
    },
        {
        'author':'python django',
        'title':'Blog Post 2',
        'context':'Second post content',
        'date_posted':'August 28, 2018',
    }
]

def home(request):
    # return HttpResponse('<h1>BMG Blog Home</h1>')
    # 用來讓使用者看到一個home page
    # request名稱可以自己設定

    # context = {'posts':posts}
    context = {'posts':Post.objects.all()}
    return render(request,'blog/home_inheritance.htm',context)


class PostListView(ListView):
    model = Post 
    #告訴這個listview class要用什麼model去建一個list

    template_name = "blog/home_inheritance.htm" 
    context_object_name = 'posts'
    ordering = ['-date_posted']


class PostDetailView(DetailView):
    model = Post
    #template_name = "blog/post_detail.html"
    # 如果沒有加的話template_name，post_detail.html是預設的template名稱


class PostCreateView(LoginRequiredMixin, CreateView):
    model = Post
    fields = ['title','context']
    template_name = "blog/post_form.html"

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

class PostUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Post
    fields = ['title','context']
    template_name = "blog/post_form.html"

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    def test_func(self):
        post = self.get_object()
        if self.request.user == post.author:
            return True
        return False

class PostDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Post
    success_url = '/'

    def test_func(self):
        post = self.get_object()
        if self.request.user == post.author:
            return True
        return False


def about(request):
    # return HttpResponse('<h1>About BMG</h1>')
    title = {'title':'About'}
    return render(request,'blog/about_inheritance.htm',title)


def home_test(request):
    context = {
        'posts':posts
    }
    return render(request,'blog/home_test.htm',context)
    #這個是沒有繼承的版本

def signup(request):
    return render(request,'blog/signup.htm')
    # render(request,'folder_in_templates/files_name')

def thank(request):
    return render(request,'blog/Thankyou.htm')
    # render(request,'folder_in_templates/files_name')

def signup_2(request):
    return render(request,'../templates/blog/signup.htm')
    # 如果把templates_folder放在porject_name的資料夾中，render的路徑就要設對
    # ..（先退到上一層）/ templates (找到templates)/folders_in_templates (找到哪個資料夾)/ files.html (找到哪個檔案)