"""bmgblog URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from users import views as user_views
from django.contrib.auth import views as auth_views
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path('admin/', admin.site.urls),
    path('blog/',include('blog.urls')),
    # 在app端設好url後也要來到上層的project端設定指向到特定app端的url
    # 導向到blog資料夾中的所有urls
    # 網址在: 網站網址或者localhost/blogs/ -> 進入blogs的urls中繼續配對裏面的連結

    path('',include('blog.urls')),
    path('register/', user_views.register, name='register'),
    path('profile/', user_views.profile, name='profile'),

    path('login/',auth_views.LoginView.as_view(template_name='users/login.htm'),name='login'),
    path('logout/',auth_views.LogoutView.as_view(template_name='users/logout.htm'),name='logout'),

    #path('/register',include('users.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    # 不直接加在上面的後面是為了讓之後人閱讀時了解這段只有在develope階段才會用到