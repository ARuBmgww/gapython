import requests
import pandas as pd
import json
import get_refresh_token
import get_grant_access
import openpyxl
import csv

# Step1. get grant_access
# Finish in get_grany_access.py

# Step2. If token expire
# Use refresh token to get access token
# Finish in get_refresh_token.py

#Load access_token in json file

def get_token():
    with open('access_token.json','r') as token_in_put:
        a1=json.load(token_in_put)
    access_token = a1['access_token']

    temp_str = ['Zoho-oauthtoken',access_token]
    Authorization = ' '.join(temp_str)
    return Authorization

def check_token(Authorization):
    zoho_organzation_url = 'https://expense.zoho.com/api/v1/organizations'
    org_header = {'Authorization':Authorization,}
    r2 = requests.get(zoho_organzation_url, headers=org_header)
    return r2.json()['code']
     

def get_organization(Authorization):
    #get Organization ID#
    zoho_organzation_url = 'https://expense.zoho.com/api/v1/organizations'
    org_header = {'Authorization':Authorization,}

    r2 = requests.get(zoho_organzation_url, headers=org_header)
    r2json = r2.json()
    org_data = r2json['organizations'][0]
    print(r2json)


#To get a list of customers#
def get_customers(Authorization):
    customers_header = {'X-com-zoho-expense-organizationid':'701024611',
                    'Authorization':Authorization}
    zoho_customers_url = 'https://expense.zoho.com/api/v1/contacts'
    r3 = requests.get(zoho_customers_url, headers=customers_header)
    r3json = r3.json()
    print(r3json)

#Get an expense report#
def get_expense_report(Authorization):
    zoho_expense_url = 'https://expense.zoho.com/api/v1/expensereports'
    expense_header = {'Content-Type':'application/json;charset=UTF-8',
                    'X-com-zoho-expense-organizationid':'701024611',
                    'Authorization':Authorization}
    r4 = requests.get(zoho_expense_url, headers=expense_header)
    r4json = r4.json()
    r4_report = r4json['expense_reports']
    print(r4json)
    return r4json


#Get an expense report Set Date#
def get_expense_report_setdate(Authorization):
    zoho_expense_url = 'https://expense.zoho.com/api/v1/reports/expensedetails'
    expense_header = {'Content-Type':'application/json;charset=UTF-8',
                    'X-com-zoho-expense-organizationid':'701024611',
                    'Authorization':Authorization}
    para = {'date_start':'2019-11-03',
            'date_end':'2019-11-30'}


    r5 = requests.get(zoho_expense_url, headers=expense_header,params=para)
    r5json = r5.json()
    #r5_report = r5json['expense_reports']
    print(r5json)
    return r5json


def create_expense_report(Authorization):
    with open('test_create_expense.json','r') as create_expense_data:
        data=json.load(create_expense_data)

    print(data)
    expense_header = {'Content-Type':'application/json;charset=UTF-8',
                    'X-com-zoho-expense-organizationid':'701024611',
                    'Authorization':Authorization}

    data2 = {
            'JSONString': '{"report_name": "test create report","description": "This report want to create areport in 2019-12-26","start_date": "2019-12-26", "end_date": "2019-12-30"}'}    
    response = requests.post('https://expense.zoho.com/api/v1/expensereports', headers=expense_header, data=data2)
    response_message(response)


def expense_report_write(ex_report):
    ex_report_keys = ex_report['expense_reports'][0].keys()
    total_report = pd.DataFrame(index=ex_report_keys)
    for temp_report in ex_report['expense_reports']:
        total_report.insert(loc=total_report.shape[1],column=temp_report['report_name'],value=list(temp_report.values()))
    
    csvfilename = 'aru_zoho_expense_report_191231.csv'
    total_report.to_csv(csvfilename, mode='w',index=True)

def expense_report_write_set_date(ex_report):
    ex_report_keys = ex_report['expenses'][0].keys()
    total_report = pd.DataFrame(index=ex_report_keys)
    for temp_report in ex_report['expenses']:
        total_report.insert(loc=total_report.shape[1],column=temp_report['report_name'],value=list(temp_report.values()))
    
    csvfilename = 'aru_zoho_expense_report_191231_setdate.csv'
    total_report.to_csv(csvfilename, mode='w',index=True)

def response_message(response):
    message = response.json()
    print(message)


if __name__ == '__main__':
    auth = get_token()
    check_code = check_token(auth)
    print(check_code)
    if check_code != '0':
        get_refresh_token.get_refresh_token()
        auth = get_token()
    #get_organization(auth)
    #create_expense_report(auth)
    #ex_report = get_expense_report(auth)
    ex_report_setdate = get_expense_report_setdate(auth)
    #expense_report_write(ex_report)
    expense_report_write_set_date(ex_report_setdate)


# write to excel #
# def expense_report_write(ex_report):
#     wb = openpyxl.Workbook()
#     report_num = len(ex_report)
#     for i in range(report_num):
#         report_name = ex_report[i]['report_name']
#         wb.create_sheet(index=i, title=report_name)

#         num = 1
#         for keys in ex_report[i].keys():
#             wb[report_name][i] = keys
#             num += 1
# not finish#

# 還是有問題#
# def expense_report_excel(ex_report):
#     report_num = len(ex_report['expense_reports'])
#     for i in range(report_num):
#         report_name = ex_report['expense_reports'][i]['report_name']
#         with open(report_name,'w') as f:
#             w = csv.writer(f)
#             w.writerow(test.keys())
#             w.writerow(test.values())
