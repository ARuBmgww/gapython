import requests
import sys
import webbrowser
import json

def open_browser():
    url = 'https://api-console.zoho.com/'
    print('Go to https://api-console.zoho.com/ to generate grant access')
    webbrowser.open(url, new=2, autoraise=True)
    code_from_web = input('Enter code get from api console: ')
    return(code_from_web)

def get_grant(code):
    ### if client and client not build_in ###
    ### download self secret put it in the main file and uncomment it ###

    # with open('self_client.json','r') as client_input:
    #     clientjson=json.load(client_input)
    # first_access_para = {'code':code, 
    #     'client_id': clientjson['client_id'],
    #     'client_secret':clientjson['client_secret'],
    #     'grant_type':'authorization_code'}
    
    ###

    first_access_para = {'code':code, 
        'client_id': '1000.EYN2RWWRXF8UM302SJ67183X15J0OH',
        'client_secret':'1a4d48970f04c0deb7b40f925a369276713fa753db',
        'grant_type':'authorization_code'}

    zoho_refreshtoken_url = 'https://accounts.zoho.com/oauth/v2/token'

    try:
        r0 = requests.post(zoho_refreshtoken_url, data=first_access_para)

    except KeyError:
        code = open_browser()
        get_grant(code)

    else:
        r0json = r0.json()
        client = {'client_id':first_access_para['client_id'],
                'client_secret':first_access_para['client_secret']}
        r0tofile = {**r0json,**client}
        with open('access_token.json','w') as token_out_put:
            json.dump(r0tofile,token_out_put)

    print('Successful write token in access_token.json')


def main():
    print('scope: ZohoExpense.fullaccess.ALL \n')
    code = open_browser()
    get_grant(code)

if __name__ == '__main__':
    main()

