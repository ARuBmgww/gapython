import requests
import json

def get_refresh_token():
    with open('access_token.json','r') as token_in_put:
        a1=json.load(token_in_put)

    refresh_para = {'refresh_token':a1['refresh_token'], 
            'client_id': a1['client_id'],
            'client_secret':a1['client_secret'],
            'grant_type':'refresh_token'}

    zoho_refreshtoken_url = 'https://accounts.zoho.com/oauth/v2/token'
    r1 = requests.post(zoho_refreshtoken_url, data=refresh_para)
    a1['access_token'] = r1.json()['access_token']

    with open('access_token.json','w') as token_out_put:
        json.dump(a1,token_out_put)


if __name__ == '__main__':
    get_refresh_token()