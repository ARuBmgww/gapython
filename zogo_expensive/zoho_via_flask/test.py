from numpy.ma.core import shape

tasks = [
    {
        'id': 1,
        'title': 'Buy groceries',
        'description': 'Milk, Cheese, Pizza, Fruit, Tylenol',
        'done': False
    },
    {
        'id': 2,
        'title': 'Learn Python',
        'description': 'Need to find a good Python tutorial on the web',
        'done': False
    }
]

task_id =1
task = list(filter(lambda t: t['id'] == task_id, tasks))

print(task)
print(len(task))
print(task_id)