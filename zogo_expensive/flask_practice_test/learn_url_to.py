from flask import Flask
from flask import url_for

# ...


app = Flask(__name__)
app.config['DEBUG'] = True

@app.route('/')
def hello():
    return 'Hello'

@app.route('/user/<name>')
def user_page(name):
    return 'User: %s' % name

@app.route('/test')
def test_url_for():
    # 下面是一些调用示例:
    print(url_for('hello')) # 输出:/
    # 注意下面两个调用是如何生成包含 URL 变量的 URL 的
    print(url_for('user_page', name='greyli'))
    x =  url_for('user_page', name='tuna')
    print(x)
    print(url_for('user_page', name='peter'))
    print(url_for('test_url_for')) # 输出:/test
    # 下面这个调用传入了多余的关键字参数,它们会被作为查询字符串附加到 URL 后面。
    print(url_for('test_url_for', num=2,key='@%fdfs$564')) # 输出:/test?num=2&key=%40%25fdfs%24564
    return 'Test page %s' %x 

if __name__ == '__main__': 
    app.run()


