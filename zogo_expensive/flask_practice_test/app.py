#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 18 12:32:06 2019

@author: bmgdev

https://flask.palletsprojects.com/en/0.12.x/quickstart/
"""

from flask import Flask
from flask import url_for
from flask.templating import render_template

app = Flask(__name__)
#載入根目錄
@app.route('/')
def index():
    return 'Index Page'

@app.route('/hello')
def hello():
    return 'welcomw to summoner''s rifts'

#動態網址
@app.route('/user/<username>')
def user_profile(username):
    # show the user profile for that user
    username = 'aru'
    return 'User %s' % username

@app.route('/post/<int:post_id>')
def show_post(post_id):
    # show the post with the given id, the id is an integer
    post_id = 777
    return 'Post %d' % post_id


@app.route('/projects/')
def projects():
    return 'The project page'

@app.route('/about')
def about():
    return 'The about page'

@app.route('/testmovie')
def testmovie():
    return render_template('index.html', name=name, movies=movies)

name = 'Aru'
movies = [
    {'title': 'My Neighbor Totoro', 'year': '1988'},
    {'title': 'Dead Poets Society', 'year': '1989'},
    {'title': 'A Perfect World', 'year': '1993'},
    {'title': 'Leon', 'year': '1994'},
    {'title': 'Mahjong', 'year': '1996'},
    {'title': 'Swallowtail Butterfly', 'year': '1996'},
    {'title': 'King of Comedy', 'year': '1999'},
    {'title': 'Devils on the Doorstep', 'year': '1999'},
    {'title': 'WALL-E', 'year': '2008'},
    {'title': 'The Pork of Music', 'year': '2012'},]

if __name__ == '__main__':
    app.run(debug=True)