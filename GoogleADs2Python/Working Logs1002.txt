## Outline
This files try to get data from Google Ads via Python.

## The reference page and code
1. https://towardsdatascience.com/getting-adwords-kpis-reports-via-api-python-step-by-step-guide-245fc74d9d73
2. https://github.com/googleads/googleads-python-lib
3. https://github.com/conan-io/conan/issues/5094

## Working Logs:
1. Set up OAuth2:
	Name: aru_goodle_test
	User id: 51xxxxxxxxxxxxxxxxxxxxxxxx.apps.googleusercontent.com  (censored)
	User password: Jxxxxxxxxxxxxxxxxxxxx (censored) 
	Set up Oauth2 DONE.
	But according to ref. the email address of OAuth client ID should be the same as the google Ads.
	Therefore I have to recreate a new OAuth2 client ID through the data2bmgww.com.

2. Get Google Ads API token
	This step need Google Ads account permission, talk to Micale for help.
	Micale help to get it. But not work properly. It is a standard level authority. 
	But in order to access Google Ads API token I need the management accountauthority.
	Change use data@bmgww.com account.
	Get the Google Ads development API: Sxxxxxxxxxxxxxxg

3. Recreate OAuth2 client ID.
	Go to Google Ads API page and create (under the project-New Adeqo Staging)
	Mame: data@bmgww2gads
	Client ID: 51xxxxxxxxxxxxxxxxxxxx.apps.googleusercontent.com 
	Client secret: Pxxxxxxxxxxxx-u 

4. Code edit and run (Get_your_refreshed_token.py)
	Copy form ref. 1.. Edit and Debug.
	Edit DEFAULT_CLIENT_ID, DEFAULT_CLIENT_SECRET, which get form "working logs 3."
	Run the code in the terminal. It will present a url, use it to login with same account (data@bmgww.com)
	After logged in would be ask to allow app (Adeqo) to access the google ads.
	After appoving, get a verification code, post to terminal and run.
	Finall, get Access token and Refresh token.

5. Download and unzip another file (ref 2.) to access to "googleleads.yaml" 
	Enter developement token, client ID, client secret.
	
6. Code edit and run (Get_ads_KPIs_HourOfDay.py)
	message: "unresolved import 'googleads' appear, use pip to install googleads.
	Install failed. message: "ERROR: Cannot uninstall 'PyYAML'...". 
	Use the method provide by (ref 3.), ignore PyYAML, and install done.
	Run and get "NameError: name 'run_campaign_performance_report' is not defined".

7. Solving error "NameError: name 'run_campaign_performance_report' is not defined".
	Annotate run_campaign_performance_report, but another error occur in Line 93. (ValueError: No objects to concatenate)
	Change funtion name to "run_Hour0fDay_kip_report", make the function can be call properly.
	But after run code again. Other new problems occur.

8. Stil solving errors.
   	Error message are below:
	raise HTTPError(req.full_url, code, msg, hdrs, fp) urllib.error.HTTPError: HTTP Error 400: Bad Request
   	raise error googleads.errors.AdWordsReportBadRequestError: Type: AuthenticationError.CLIENT_CUSTOMER_ID_INVALID Trigger: <null> Field Path: None
	Traceing line by line, now in Line "adwords_client = adwords.AdWordsClient.LoadFromStorage('./googleads.yaml')" appear error "無法載入來源 '<attrs generated init e33a5e0e06618ceb6a201f2a88c03287f08a1742>': Source unavailable。"
	Finding ways to solving...

## Result
The code still has some problems, so it can't run well now. 

## Disscussion and Further work:
1. I think the errors may occur in previous step, while getting Google Ads token and api authorization. Because that step quite complicated.
2. Recheck the credental whether it get the correct premission.
3. Rerun the code.
4. check Google Ads API manauls to modify.
