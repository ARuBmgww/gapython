from __future__ import print_function
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from oauth2client.service_account import ServiceAccountCredentials
from pprint import pprint
from googleapiclient import discovery
import pandas as pd

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/spreadsheets']

# The ID and range of a sample spreadsheet.
SAMPLE_SPREADSHEET_ID = '1XkBnubnPoO50sYpVw77CzmegTS8Y0jSH-08-nFfA5YU'
SAMPLE_RANGE_NAME = 'Sheet11'

creds = None

if os.path.exists('token.pickle'):
    with open('token.pickle', 'rb') as token:
        creds = pickle.load(token)

if not creds or not creds.valid:
    if creds and creds.expired and creds.refresh_token:
        creds.refresh(Request())
    else:
        flow = InstalledAppFlow.from_client_secrets_file(
            'credentials.json', SCOPES)
        creds = flow.run_local_server(port=0)

    with open('token.pickle', 'wb') as token:
        pickle.dump(creds, token)


csvfile_name = 'Slim_Beauty_GADS_TEST.csv'
df = pd.read_csv(csvfile_name,low_memory=False, na_values=[' --'],dtype=str)


temp = [[12,12,31,],[1,3,6,8,]]
service = build('sheets', 'v4', credentials=creds)
spreadsheet_id = SAMPLE_SPREADSHEET_ID 
range_ = SAMPLE_RANGE_NAME  
value_input_option = 'RAW'
insert_data_option = 'INSERT_ROWS'
value_range_body = {
    'values':temp
    }        
request = service.spreadsheets().values().append(spreadsheetId=spreadsheet_id, range=range_, valueInputOption=value_input_option, insertDataOption=insert_data_option, body=value_range_body)
response = request.execute()
pprint(response)

