from __future__ import print_function
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from oauth2client.service_account import ServiceAccountCredentials
import gspread
from pprint import pprint
from googleapiclient import discovery

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/spreadsheets']

creds = None
if os.path.exists('token.pickle'):
    with open('token.pickle', 'rb') as token:
        creds = pickle.load(token)

if not creds or not creds.valid:
    if creds and creds.expired and creds.refresh_token:
        creds.refresh(Request())
    else:
        flow = InstalledAppFlow.from_client_secrets_file(
            'credentials.json', SCOPES)
        creds = flow.run_local_server(port=0)

    with open('token.pickle', 'wb') as token:
        pickle.dump(creds, token)


SAMPLE_SPREADSHEET_ID = '1XkBnubnPoO50sYpVw77CzmegTS8Y0jSH-08-nFfA5YU'
SAMPLE_RANGE_NAME = 'Google Spending!A1:E1'

service = build('sheets', 'v4', credentials=creds)
spreadsheet = {'properties': {'title': '1028t'},
                'sheets':[
                        {'properties': {'index':0,'title': 'test01'}},
                        {'properties': {'index':1,'title': 'test02'}}
                            ]
                }
spreadsheet = service.spreadsheets().create(body=spreadsheet,fields='spreadsheetId').execute()
print('Spreadsheet ID: {0}'.format(spreadsheet.get('spreadsheetId')))