'''
This test is to fully understand the following code.
And run it  step by step.
'''
import pandas as pd
import numpy as np
import datetime
import io
import os
import glob
import sys
from googleads import adwords
from datetime import datetime, timedelta

start_date=datetime.today().date().isoformat().replace("-", "")
end_date=datetime.now() + timedelta(days= - 5)
end_date= end_date.date().isoformat().replace("-","")

# key = 'Slim Beauty'
# acc_id = '713-383-2798'

key = 'MTM Skincare'
acc_id = '970-480-4020'

# Define output as a string
output= io.StringIO()

adwords_client = adwords.AdWordsClient.LoadFromStorage('./googleads.yaml')
adwords_client.SetClientCustomerId(acc_id)
report_downloader = adwords_client.GetReportDownloader(version='v201809')

campaign_extension_setting_service = adwords_client.GetService('CampaignExtensionSettingService', version='v201809')
customer_service = adwords_client.GetService('CustomerService', version='v201809')
customer = customer_service.getCustomers()[0]
customer_currencycode = customer['currencyCode']


report_query = (adwords.ReportQueryBuilder()
    .Select('Date','CampaignId', 'CampaignName', 'Cost')
    .From('CAMPAIGN_PERFORMANCE_REPORT')
    .Where('CampaignStatus').In('ENABLED')
    .During(end_date+ ','+start_date).Build())
    
report_downloader.DownloadReportWithAwql(report_query, 'CSV', output, 
    skip_report_header=True, skip_column_header=False, 
    skip_report_summary=True, include_zero_impressions=False)

output.seek(0)
    
types= {'CampaignId':pd.np.int64, 'Cost': pd.np.float64}

df = pd.read_csv(output,low_memory=False, dtype= types, na_values=[' --'])
# delete the first and last column
df.insert(loc=1,column='Account',value=key,allow_duplicates=True)
df.insert(loc=1,column='Currency Code',value=customer_currencycode,allow_duplicates=True)
# micro amount 1000000
df['Cost']=df.Cost/1000000
df['Cost']=df['Cost'].round(2)
df = df[['Day','Account','Campaign ID','Campaign','Currency Code','Cost']]
print(df.head())
df.to_csv('MTM Skincare_GADS_TEST.csv', mode='w',index=False)


