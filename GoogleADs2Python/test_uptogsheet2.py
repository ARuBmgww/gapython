import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

# https://developers.google.com/sheets/api/guides/authorizing

SCOPES = ['https://www.googleapis.com/auth/spreadsheets']

SHEET_NAME = 'TESTING MTM | Budget Tracker'
SPREADSHEET_ID = '1ik3xcCT9zRPslvfdVCgZnW1uAyATg1qpkL4-2ApH3PI'

CREDENTIAL_FILE = 'credentials.json'
TOKEN_FILE = 'token.pickle'

credentials = None


if os.path.exists(TOKEN_FILE):
    with open(TOKEN_FILE, 'rb') as token:
        credentials = pickle.load(token)

if not credentials or not credentials.valid:
    if credentials and credentials.expired and credentials.refresh_token:
        credentials.refresh(Request())
    else:
        flow = InstalledAppFlow.from_client_secrets_file(
            CREDENTIAL_FILE, SCOPES)
        credentials = flow.run_local_server(port=0)
    # Save the credentials for the next run
    with open(TOKEN_FILE, 'wb') as token:
        pickle.dump(credentials, token)

service = build('sheets', 'v4', credentials=credentials)
sheet = service.spreadsheets()

range_notation = 'Google Spending!B1:C10'
result = sheet.values().get(spreadsheetId=SPREADSHEET_ID,
                            range=range_notation).execute()
rows = result.get('values', [])

if not rows:
    print('No data found.')
else:
    for row in rows:
        print(row)