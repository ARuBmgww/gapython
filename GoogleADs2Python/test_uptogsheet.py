from __future__ import print_function
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from oauth2client.service_account import ServiceAccountCredentials
import gspread
from pprint import pprint
from googleapiclient import discovery

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/spreadsheets']

# The ID and range of a sample spreadsheet.
SAMPLE_SPREADSHEET_ID = '1XkBnubnPoO50sYpVw77CzmegTS8Y0jSH-08-nFfA5YU'
SAMPLE_RANGE_NAME = 'Google Spending!A1:E1'

def run_credentials():
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)
    return(creds)

def create_sheet(sheetname,creds):
    #create a new sheet named '191028test'. And print the sheet_id
    service = build('sheets', 'v4', credentials=creds)
    spreadsheet = {'properties': {'title': sheetname},
                    'sheet':[{'properties': {'title': 'test01'}}]}
    spreadsheet = service.spreadsheets().create(body=spreadsheet,fields='spreadsheetId').execute()
    print('Spreadsheet ID: {0}'.format(spreadsheet.get('spreadsheetId')))




def main():
    """Shows basic usage of the Sheets API.
    Prints values from a sample spreadsheet.
    """
    #Get files token
    creds = run_credentials()
    service = build('sheets', 'v4', credentials=creds)

    #Create sheet(sheetname,creds)
    #create_sheet('10288test',creds)
    
    # Call the Sheets API
    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=SAMPLE_SPREADSHEET_ID,
                                range=SAMPLE_RANGE_NAME).execute()
    values = result.get('values', [])

    if not values:
        print('No data found.')
    else:
        print('Name, Major:')
        for row in values:
            # Print columns A and E, which correspond to indices 0 and 4.
            print('%s, %s' % (row[0], row[4]))

    # The spreadsheet to request.
    spreadsheet_id = 'SAMPLE_SPREADSHEET_ID'  # TODO: Update placeholder value.

    # The ranges to retrieve from the spreadsheet.
    ranges = []  # TODO: Update placeholder value.

    # True if grid data should be returned.
    # This parameter is ignored if a field mask was set in the request.
    include_grid_data = False  # TODO: Update placeholder value.

    request = service.spreadsheets().get(spreadsheetId=SAMPLE_SPREADSHEET_ID, ranges=ranges, includeGridData=include_grid_data)
    response = request.execute()

    # TODO: Change code below to process the `response` dict:
    pprint(response)


if __name__ == '__main__':
    main()