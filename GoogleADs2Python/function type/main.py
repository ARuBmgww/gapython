import get_googleads_data
import upload_to_google_sheets
import upload_to_servers

def main():
    #Google ADs account 
    key1 = 'Slim Beauty'
    acc_id1 = '713-383-2798'

    key2 = 'MTM Skincare'
    acc_id2 = '970-480-4020'

    #Get Google ADs data, generate a csv files 
    csvname1 = get_googleads_data.main(key1,acc_id1)
    csvname2 = get_googleads_data.main(key2,acc_id2)

    #Enter sheet id and workspace name
    SPREADSHEET_ID_slim = '1XkBnubnPoO50sYpVw77CzmegTS8Y0jSH-08-nFfA5YU'
    RANGE_NAME_slim = 'Google Spending'
    SPREADSHEET_ID_mtm = '1ik3xcCT9zRPslvfdVCgZnW1uAyATg1qpkL4-2ApH3PI'
    RANGE_NAME_mtm = 'Google Spending'

    #upload to google sheet
    upload_to_google_sheets.main(SPREADSHEET_ID_slim,RANGE_NAME_slim,csvname1)
    upload_to_google_sheets.main(SPREADSHEET_ID_mtm,RANGE_NAME_mtm,csvname2)

    #upload to posgres
    upload_to_servers.main(csvname1)
    upload_to_servers.main(csvname2)


if __name__ == '__main__':
    main()
