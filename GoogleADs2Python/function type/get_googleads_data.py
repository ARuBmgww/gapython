'''
This test is to fully understand the following code.
And run it  step by step.
'''

import pandas as pd
import numpy as np
import datetime
import io
import os
import glob
import sys
from googleads import adwords
from datetime import datetime, timedelta, date

def time_log():
    timelog = datetime.now().isoformat(sep=' ')
    return timelog

def main(key,acc_id):
    account = 'Google ADs'
    start_date=datetime.today().date() + timedelta(days=-1)
    start_date=start_date.isoformat().replace("-", "")
    end_date=datetime.now().date() + timedelta(days=-1)
    #end_date=date(2019,11,1)
    end_date= end_date.isoformat().replace("-","")

    # Define output as a string

    output= io.StringIO()

    adwords_client = adwords.AdWordsClient.LoadFromStorage('./googleads.yaml')
    adwords_client.SetClientCustomerId(acc_id)
    report_downloader = adwords_client.GetReportDownloader(version='v201809')

    report_query = (adwords.ReportQueryBuilder()
        .Select('Date','CampaignId', 'CampaignName', 'Cost','AccountCurrencyCode')
        .From('CAMPAIGN_PERFORMANCE_REPORT')
        .Where('CampaignStatus').In('ENABLED')
        .During(end_date+ ','+start_date).Build())
    
    report_downloader.DownloadReportWithAwql(report_query, 'CSV', output, 
        skip_report_header=True, skip_column_header=False, 
        skip_report_summary=True, include_zero_impressions=False)

    output.seek(0)  
    types= {'CampaignId':pd.np.int64, 'Cost': pd.np.float64}
    df = pd.read_csv(output,low_memory=False, dtype= types, na_values=[' --'])
    # delete the first and last column
    df.insert(loc=1,column='channel',value=account,allow_duplicates=True)
    df.insert(loc=df.shape[1],column='created_at',value=time_log(),allow_duplicates=True)
    # micro amount 1000000
    df['Cost']=df.Cost/1000000
    df['Cost']=df['Cost'].round(2)
    df.rename(columns={'Day':'date','Campaign ID':'campaign_id','Campaign':'campaign_name','Cost':'cost'},inplace=True)
    print(df.head())
    csvfilename = key+'_yesterday.csv'
    df.to_csv(csvfilename, mode='w',index=False)
    return csvfilename

if __name__ == '__main__':
    main()


