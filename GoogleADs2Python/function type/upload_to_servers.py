import psycopg2
import csv
from datetime import datetime, timedelta


def connect2adeqo():
    connect = psycopg2.connect(database="adeqo_pro", user="postgres", password="", host="adeqo.com", port="5432")
    print('Opened database successfully')
    cur = connect.cursor()
    return cur, connect

def connect2psql():
    connect = psycopg2.connect(database="arutestac", user="aru", password="", host="127.0.0.1", port="5432")
    print('Opened database successfully')
    cur = connect.cursor()
    return cur, connect

def comma_process(ENTERstr):
    daylist = list(ENTERstr)
    daylist.insert(0,"'")
    daylist.append("'")
    daystr2 = ''.join(daylist)
    return daystr2

def time_log():
    timelog = datetime.now().isoformat(sep=' ')
    return timelog

def table_create(cur):
    cur.execute('''CREATE TABLE campaign_statistics (
        date    DATE, 
        channel   varchar, 
        campaign_id      varchar, 
        campaign_name     varchar, 
        CurrencyCode  varchar, 
        cost          double precision, 
        created_at    timestamp without time zone,
        updated_at    timestamp without time zone)''')

def add_new_column(cur):
    cur.execute('ALTER TABLE campaign_statistics ADD Currency varchar')

def main(file):
    #cur1, connect1 = connect2psql()
    cur2, connect2 = connect2adeqo()
    sql_insert = '''INSERT INTO campaign_statistics (date , channel, campaign_id, campaign_name, 
                cost, Currency ,created_at, updated_at) 
                VALUES(%s,%s,%s,%s,%s,%s,%s,%s)'''

    with open(file, 'r') as f:
        reader = csv.reader(f)
        next(reader) # This skips the 1st row which is the header.
        csvlist = list(reader)

    #table_create(cur1)
    #table_create(cur2)
    #add_new_column(cur2)
    for i in csvlist:
        temp = i
        temp.append(time_log())
        temp[0] = comma_process(temp[0])
        temp[1] = comma_process(temp[1])
        temp[3] = comma_process(temp[3])
        temp[5] = comma_process(temp[5])
        temp[6] = comma_process(temp[6])
        temp[7] = comma_process(temp[7])
        #cur1.execute(sql_insert %(temp[0],temp[1],temp[2],temp[3],temp[4],temp[5],temp[6],temp[7]))
        cur2.execute(sql_insert %(temp[0],temp[1],temp[2],temp[3],temp[4],temp[5],temp[6],temp[7]))

    #connect1.commit()
    #connect1.close()
    connect2.commit()
    connect2.close()
    print('Uploaded to database successfully')

if __name__ == '__main__':
    main()

