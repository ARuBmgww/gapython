from __future__ import print_function
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from oauth2client.service_account import ServiceAccountCredentials
from pprint import pprint
from googleapiclient import discovery


# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/spreadsheets']

creds = None

if os.path.exists('token.pickle'):
    with open('token.pickle', 'rb') as token:
        creds = pickle.load(token)

if not creds or not creds.valid:
    if creds and creds.expired and creds.refresh_token:
        creds.refresh(Request())
    else:
        flow = InstalledAppFlow.from_client_secrets_file(
            'credentials.json', SCOPES)
        creds = flow.run_local_server(port=0)

    with open('token.pickle', 'wb') as token:
        pickle.dump(creds, token)


SAMPLE_SPREADSHEET_ID = '1XkBnubnPoO50sYpVw77CzmegTS8Y0jSH-08-nFfA5YU'
SAMPLE_RANGE_NAME = 'Google Spending!A1:E1'

# The spreadsheet to request.
spreadsheet_id = 'SAMPLE_SPREADSHEET_ID'  # TODO: Update placeholder value.
service = build('sheets', 'v4', credentials=creds)
# The ranges to retrieve from the spreadsheet.
ranges = []  # TODO: Update placeholder value.

# True if grid data should be returned.
# This parameter is ignored if a field mask was set in the request.
include_grid_data = False  # TODO: Update placeholder value.

request = service.spreadsheets().get(spreadsheetId=SAMPLE_SPREADSHEET_ID, ranges=ranges, includeGridData=include_grid_data)
response = request.execute()

# TODO: Change code below to process the `response` dict:
#pprint(response)

temp = response
all_title = []
for i in range(len(temp.get('sheets'))):
    cc = temp.get('sheets')[i].get('properties').get('title')
    all_title.append(cc)

print(all_title)