
#%%
import pandas as pd
import numpy as np
import random

learn_insert_apply = pd.DataFrame(np.random.rand(4,3),columns=['a','b','c'],index=['ind0','ind1','ind2','ind3'])
learn_insert_apply2 = learn_insert_apply
print(learn_insert_apply2)

learn_insert_apply2.insert(loc=0,column='testdata',value=learn_insert_apply['c'],allow_duplicates=True)
print(learn_insert_apply2)

learn_insert_apply2 = learn_insert_apply
learn_insert_apply2.insert(loc=0,column='testdata2',value=learn_insert_apply2['c'].apply(lambda x:x+2))
print(learn_insert_apply2)

#%%
'''
df is a dataframe. use "list(df.columns)" and "list(df.index)" to check column and row name.
There is only one column:"report", and only one row:"0".
When get data from dataframe, needs add "[0]" to get row vector.

type(df) = pandas.core.frame.DataFrame
    column:"report", row:"0"
type(tt1) = dict
    dict_keys(['columnHeader', 'data'])
type(tt2) = dict
    dict_keys(['rows', 'totals', 'rowCount', 'minimums', 'maximums'])
type(tt3) = list
    tt3[x], where x belong to integers
    Ex: tt3[0] = {'dimensions': ['20191001'], 'metrics': [{'values': ['60']}]}

Finally we got the 'dimensionsga'(ga:Date),and 'values'
'''

response= {'Date': ['2019-10-01', '2019-10-02', '2019-10-03'], 'response': [{'reports': [{'columnHeader': {'metricHeader': {'metricHeaderEntries': [{'name': 'ga:CTR', 'type': 'PERCENT'}]}}, 'data': {'rows': [{'metrics': [{'values': ['3.8922155688622757']}]}], 'totals': [{'values': ['3.8922155688622757']}], 'rowCount': 1, 'minimums': [{'values': ['3.8922155688622757']}], 'maximums': [{'values': ['3.8922155688622757']}], 'isDataGolden': True}}]}, {'reports': [{'columnHeader': {'metricHeader': {'metricHeaderEntries': [{'name': 'ga:CTR', 'type': 'PERCENT'}]}}, 'data': {'rows': [{'metrics': [{'values': ['3.1446540880503147']}]}], 'totals': [{'values': ['3.1446540880503147']}], 'rowCount': 1, 'minimums': [{'values': ['3.1446540880503147']}], 'maximums': [{'values': ['3.1446540880503147']}], 'isDataGolden': True}}]}, {'reports': [{'columnHeader': {'metricHeader': {'metricHeaderEntries': [{'name': 'ga:CTR', 'type': 'PERCENT'}]}}, 'data': {'rows': [{'metrics': [{'values': ['3.3542976939203357']}]}], 'totals': [{'values': ['3.3542976939203357']}], 'rowCount': 1, 'minimums': [{'values': ['3.3542976939203357']}], 'maximums': [{'values': ['3.3542976939203357']}], 'isDataGolden': True}}]}]}
data = pd.DataFrame(response)
tt1 = data.get('response', [])[0]
tt2 = data.get('response', []).get('reports', []).get('data')
tt3 = data.get('response',[])[0].get('reports', [])[0].get('data').get('rows')



#%%
'''
After getting the correct position, then it needs to modify
the code here would vary with the ga:XXX data type 
For example, ga:userAgeBracket (in gadata191002.py) have 2 dimension "['18-24', 'female']" (show in ex3)
so it needs ex4.insert(loc=0,column='Age',value=ex4['dimensions'].apply(lambda x:x[0]))
to retrieve one dimension first.
but in ga:Date, have only one dimension. It doesn't need that code.

Below are two part. 
ex4 is the demo code provided by internet
tt4 is modified by myself base on ex4, it can help to retrieve other data.

tt4['metrics'] = tt4['metrics'].apply(lambda x:x[0]).apply(lambda x:x.get('values',[])).apply(lambda x:x[0])
this code can separate to different part

tt4
    dimensions	metrics
0	[20191001]	[{'values': ['60']}]
1	[20191002]	[{'values': ['57']}]
2	[20191003]	[{'values': ['51']}]
Name: metrics, dtype: object

tt4['metrics']
0    [{'values': ['60']}]
1    [{'values': ['57']}]
2    [{'values': ['51']}]
Name: metrics, dtype: object

"previous".apply(lambda x:x[0])
    dimensions	metrics
0	[20191001]	{'values': ['60']}
1	[20191002]	{'values': ['57']}
2	[20191003]	{'values': ['51']}

"previous".apply(lambda x:x.get('values',[]))
    dimensions	metrics
0	[20191001]	[60]
1	[20191002]	[57]
2	[20191003]	[51]

"previous".apply(lambda x:x[0])
    dimensions	metrics
0	[20191001]	60
1	[20191002]	57
2	[20191003]	51

finally we have clean up the result.
'''
tt4 = pd.DataFrame(tt3)
time_serial = ['2019-10-07']
tt4['metrics'] = tt4['metrics'].apply(lambda x:x[0]).apply(lambda x:x.get('values',[])).apply(lambda x:x[0])

tt4.rename(columns={'metrics':'Number',},inplace= True)


#%%
'''
write data to a csv files
index = False       don't print row index.
line_terminal='\n'  when line in end, move to next line
sep = '\t'          Field delimiter. separate cell by tab
'''
tt4.to_csv('date.csv',index=False,line_terminator='\n',sep='\t')


#%%
