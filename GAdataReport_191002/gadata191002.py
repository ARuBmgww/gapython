import argparse
from googleapiclient.discovery import build
import httplib2
from oauth2client import client
from oauth2client import file
from oauth2client import tools
import pandas as pd
import csv

SCOPES = ['https://www.googleapis.com/auth/analytics.readonly']
DISCOVERY_URI = ('https://analyticsreporting.googleapis.com/$discovery/rest')
CLIENT_SECRETS_PATH = 'client_secret.json' #replace with the path to JSON File
VIEW_ID = '107115309' #replace the view ID#

def initialize_analyticsreporting():
  parser = argparse.ArgumentParser(
      formatter_class=argparse.RawDescriptionHelpFormatter,
      parents=[tools.argparser])
  flags = parser.parse_args([])
  flow = client.flow_from_clientsecrets(
      CLIENT_SECRETS_PATH, scope=SCOPES,
      message=tools.message_if_missing(CLIENT_SECRETS_PATH))
  storage = file.Storage('analyticsreporting.dat')
  credentials = storage.get()
  if credentials is None or credentials.invalid:
    credentials = tools.run_flow(flow, storage, flags)
  http = credentials.authorize(http=httplib2.Http())
  analytics = build('analytics', 'v4', http=http, discoveryServiceUrl=DISCOVERY_URI)
  return analytics

def get_report(analytics):
  return analytics.reports().batchGet(
      body={
        'reportRequests': [
        {
          'viewId': VIEW_ID,
          'dateRanges': [{'startDate': '7daysAgo', 'endDate': 'today'}],
          'dimensions': [{'name': 'ga:userAgeBracket'}, {'name': 'ga:userGender'}], #our new dimensions
          'metrics': [{'expression': 'ga:sessions'}]
        }]
      }
  ).execute()

def print_response(response):
    data = response.get('reports', [])[0].get('data', {}).get('rows', [])
    df = pd.DataFrame(data)
    df.insert(loc=0, column="age", value=df['dimensions'].apply(lambda x: x[0]))
    df['dimensions'] = df['dimensions'].apply(lambda x: x[1])
    df['metrics'] = df['metrics'].apply(lambda x: x[0]).apply(lambda x: x.get('values')).apply(lambda x: x[0])
    df.rename(columns={'dimensions': 'gender', 'metrics': 'sessions'}, inplace=True)
    return df


def main():
  analytics = initialize_analyticsreporting()
  response = get_report(analytics) #get the response from the API
  print(print_response(response)) #print the response from the API

if __name__ == '__main__':
  main()