import argparse
from googleapiclient.discovery import build
import httplib2
from oauth2client import client
from oauth2client import file
from oauth2client import tools
import pandas as pd
import csv
from datetime import timedelta, date , datetime

SCOPES = ['https://www.googleapis.com/auth/analytics.readonly']
DISCOVERY_URI = ('https://analyticsreporting.googleapis.com/$discovery/rest')
CLIENT_SECRETS_PATH = 'client_secret_aru.json' #replace with the path to JSON File
VIEW_ID = '107115309' #replace the view ID#

def initialize_analyticsreporting():
  parser = argparse.ArgumentParser(
      formatter_class=argparse.RawDescriptionHelpFormatter,
      parents=[tools.argparser])
  flags = parser.parse_args([])
  flow = client.flow_from_clientsecrets(
      CLIENT_SECRETS_PATH, scope=SCOPES,
      message=tools.message_if_missing(CLIENT_SECRETS_PATH))
  storage = file.Storage('analyticsreporting.dat')
  credentials = storage.get()
  if credentials is None or credentials.invalid:
    credentials = tools.run_flow(flow, storage, flags)
  http = credentials.authorize(http=httplib2.Http())
  analytics = build('analytics', 'v4', http=http, discoveryServiceUrl=DISCOVERY_URI)
  return analytics

def get_report(analytics,timeserial):
      return analytics.reports().batchGet(
      body={
        'reportRequests': [
        {
          'viewId': VIEW_ID,
          'dateRanges': [{'startDate': timeserial, 'endDate': timeserial}],
          #'dimensions':[{'name':'ga:Cost'}],
          'metrics': [
          {'expression': 'ga:adCost'},
          #{'expression': 'ga:CPC'},
          #{'expression': 'ga:adClicks'},
          #{'expression': 'ga:impressions'},
          #{'expression': 'ga:CTR'},
          #{'expression': 'ga:costPerGoalConversion'},
          #{'expression': 'ga:users'}
          ]
        }]
      }
  ).execute()

def print_response(rawdata):
      data = pd.DataFrame(rawdata)
      data['response'] = data['response'].apply(lambda x:x.get('reports')[0].get('data'))
      data['response'] = data['response'].apply(lambda x:x.get('totals') if x.get('rows','none')=='none' else x.get('rows')[0].get('metrics'))
      data['response'] = data['response'].apply(lambda x:x[0].get('values')[0])
      return data

def daterange(start_date, end_date):
      for n in range(int ((end_date - start_date).days)+1):
            yield start_date + timedelta(n)
    

def main():
    analytics = initialize_analyticsreporting()
    start_date = date(2019,9,5)
    end_date = date(2019,9,10)
    tempdata =[]
    temptime =[]
    time_serial = [start_date.strftime("%Y-%m-%d")]
    for single_date in daterange(start_date, end_date):
          time_serial = single_date.strftime("%Y-%m-%d")
          response = get_report(analytics,time_serial) #get the response from the API
          temptime.append(time_serial)
          tempdata.append(response)
    rawdata = {'Date':temptime,'response':tempdata}
    #print(time_serial)
    print(rawdata)
    #df = print_response(rawdata)
    #print(df) #print the response from the API
    #df.to_csv('test_exist_date.csv',index=False,line_terminator='\n',sep='\t')

if __name__ == '__main__':
  main()