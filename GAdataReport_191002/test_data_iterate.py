from datetime import timedelta, date

def daterange(start_date, end_date):
    for n in range(int ((end_date - start_date).days)):
        yield start_date + timedelta(n)

start_date = date(2016, 1, 1)
end_date = date(2016, 6, 2)
time_serial =[start_date.strftime("%Y-%m-%d")]
for single_date in daterange(start_date, end_date):
    print(single_date.strftime("%Y-%m-%d"))
    time_serial.append(single_date.strftime("%Y-%m-%d"))


