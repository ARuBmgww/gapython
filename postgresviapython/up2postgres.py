
import psycopg2
import csv
connect = psycopg2.connect(database="arutestac", user="aru", password="k1111111119", host="127.0.0.1", port="5432")
print('Opened database successfully')
cur = connect.cursor()

def dayprocess(daystr):
    daylist = list(daystr)
    daylist.insert(0,"'")
    daylist.append("'")
    daystr2 = ''.join(daylist)
    return daystr2


def table_create():
    cur.execute('''CREATE TABLE GA_DATA (
        ga_Date     DATE, 
        ga_adCost   REAL, 
        ga_CPC      REAL, 
        ga_adClicks     REAL, 
        ga_impressions  REAL, 
        ga_CTR          REAL, 
        ga_costPerGoalConversion REAL, 
        ga_users    REAL)''')

sql_insert = '''INSERT INTO GA_DATA (ga_Date , ga_adCost, ga_CPC, ga_adClicks, ga_impressions, ga_CTR, ga_costPerGoalConversion, ga_users) 
            VALUES(%s,%s,%s,%s,%s,%s,%s,%s)'''

file = r'test_exist_date_yesterday.csv'

with open(file, 'r') as f:
    reader = csv.reader(f)
    next(reader) # This skips the 1st row which is the header.
    csvlist = list(reader)

#table_create()
for i in csvlist:
    temp = i[0].split('\t')
    dateinsert = dayprocess(temp[0])
    cur.execute(sql_insert %(dateinsert,temp[1],temp[2],temp[3],temp[4],temp[5],temp[6],temp[7]))

connect.commit()
connect.close()