'''
def fact(n)
    if n ==1 
        1
    else
        n * fact(n-1)
    end
end
puts (ARGV)
puts (ARGV[0])
puts(fact(ARGV[0].to_i))  
'''
# ARGV包含了所有從終端機輸入的參數，因為是字串所以要是.to_i轉成數字
# 以上的程式如果輸入的第一個不是數字。例如 ruby fact.rb t55
# 這樣就會出現錯誤
# 以下改寫成由使用者自行輸入要乘的階乘

def fact(n)
    if n == 0
        1
    else
        n*fact(n-1)
    end
end
print 'Enter a num: '
n = gets.chomp().to_i
puts("The #{n}! is " + fact(n).to_s)
