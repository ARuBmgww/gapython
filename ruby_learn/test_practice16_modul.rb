module Tools
    def sayhi(name)
        puts "hello #{name}"
    end

    def saybye(name)
        puts "by #{name}"
    end
end

include Tools
Tools.sayhi("Aru")

# 建立模組要用 module Xxx
#               ...
#            end
#
#用 include Xxx 來使用
#
#如果要使用其他 .rb 檔案中的 Xxx module
#在主程式中用 require_relative "xxx.rb"
#           include Xxx