lucky_num = [0,2,3,5,9]

begin
    #num = 10/0
    lucky_num["dog"]

rescue ZeroDivisionError #若只有用rescue 就會所有錯誤都抓，但加上特定error其他的就抓不到
    puts "Division by zero error"

rescue TypeError => e # 把錯誤存在 e，發生錯誤時就能有細節
    puts "Wrong Type"
    puts e
end