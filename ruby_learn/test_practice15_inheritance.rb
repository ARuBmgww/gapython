# inheritance 可以擴大一個class的功能到其他class，組成一個super class

class Chef
    def make_chicken
        puts "The chef makes chicken"
    end

    def make_salad
        puts "The chef makes salad"
    end

    def make_special_dish
        puts "The chef makes bbq ribs"
    end
end

class ItalianChef < Chef # 這樣就把所有Chef中的屬性都繼承到 IntalianChef中
    
    def make_special_dish
        puts "The chef makes Pizza" # 名稱跟原本繼承的取一樣就會覆蓋掉。重新定義了
    end

    def make_pasta  # 一樣可以創立新的方法，但是不會傳回去原本繼承的 class
        puts "The chef makes pasta"
    end
end

chef = Chef.new()
chef.make_chicken

italian_chef = ItalianChef.new()
italian_chef.make_special_dish
italian_chef.make_pasta
