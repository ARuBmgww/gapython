# 對ruby來說，所有的資料都是object，都可以有個對應的class儲存資料格式
# 建立一個book的class，class是一個客制化的資料格式
# 就像是一個代表 book 的藍圖
# attr_accessor 用來設置所有在程式中的 book 應該有的屬性
# 建立class 的名字規定必須是常數，也就是必須是大寫英文字母開頭

class Book

    attr_accessor :title, :author, :pages

    def initialize(title_in, author_in, pages_in)
        @title = title_in
        @author = author_in
        @pages = pages_in
        puts "Creating Book attribute succeed"
    end
    # def一個initialize，每次只要用new()建立時都會呼叫這個程式
    # 用@ 把傳進來的參數傳遞到 attr_acccessor中
end

# 在沒有 initialize 的情況下可以如以下方式建立屬性#
# 但是這樣太費工了， 因此會在class中 建立一個初始化的副程式

# book1 = Book.new() 
# book1.title = "Happy Potter"
# book1.author =  "JK Rowling"
# book1.pages = "400"

# puts book1.title
# puts book1.author
# puts book1.pages


book2 = Book.new("The Lord of the Ring", "Tolkein", "500")

puts book2.title
puts book2.author
puts book2.pages