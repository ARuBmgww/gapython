# case 一對多比較免去重複使用if判別的問題

def get_day_name(day)
    day_name = ""

    case day
    when "mon"
        day_name = "Momday"
    when "tue"
        day_name = "Tuesday"
    when "wed"
        day_name = "Wednsday"
    when "thu"
        day_name = "Thursday"
    when "fri"
        day_name = "Friday"
    when "sat"
        day_name = "Satursday"
    when "sun"
        day_name = "Sunday"
    else
        day_name = "Invalid abbreviation"
    end

    return day_name
end

puts get_day_name("sat")
puts get_day_name("dog")