File.open('pickuplist.txt', 'r') do |file_open| #把檔案打開後存到 file_open, File.open() 要大寫
    puts file_open
    puts file_open.readline()
    puts file_open.readline()
    puts (file_open.read().include? "4-7'")
end

#-- 另一種開檔案的方法 但是要加上 zzz.close() 關上檔案--#
test_open = File.open("pickuplist.txt", "r")
puts (test_open.readline())

for line in test_open.readlines()
    puts line
end
test_open.close()


#-- Write file --#
File.open('test_html.html', 'w') do |file|
    file.write("<h1>hello</h1>")
end


#-- Read and Write file --#
File.open('test_html.html', 'r+') do |file|
    file.readline()
    file.write("\nOverridden")
end