# frozen_string_literal: true

class Student
    attr_accessor :name, :major, :gpa
    def initialize(name_in, major_in, gpa_in)
        @name = name_in
        @major = major_in
        @gpa = gpa_in
        puts 'Creating Book attribute succeed'
    end

    # 在class中建立方法，外面就可以讓建立的物件直接使用這個方法
    def has_honor
        if @gpa >= 3.5 # 引用參數別忘了加上@
            return true
        end
        return false
    end
end

student1 = Student.new("Jim", "Business", 2.6)
student2 = Student.new("Pam", "Art", 3.6)

puts student1.has_honor
puts student2.has_honor
