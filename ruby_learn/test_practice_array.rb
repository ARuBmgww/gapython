#Pratice array

friend = Array["Yu Yu","Bi Bi", "May", "Bay"]
puts friend[0,2]

women = Array.new
women[0] = "Bay"
women[2] = "zoy"
women[1] = 1

puts women
puts women.length()
puts women.include? "Bay"
puts women.reverse()
# puts women.sort() #文字無法排列，所以會出錯

#Pratice hash (類似dict)
city = {
    "Taipei" => "TPE",
    "Tainan" => "TN",
    "Kaoshung" => "KO",
    :Taichung => "TC" #用冒號跟用雙引號一樣
}

puts city["Taipei"]
puts city[:Taichung]

city["Pingtung"] = "PG"
puts city
puts city["Pingtung"]