friends = ["Kevin", "Andy", "Oscar", "Angela", "Elsa"]

for name in friends
    puts name
end

# 下面這種寫法跟上面的意思一樣 #
friends.each do |name|
    puts name
end

######

for index in 0..8 # 表示從0到8
    puts index
end

#---這兩種寫法的意思都一樣#
#但是只會列出0到7
#所以其實要等同上面的寫法是 0..7

8.times do |index|
    puts index
end

######

# 範例 次方器
def pow(base_num, pow_num)
    result = 1

    pow_num.times do |index| # 本次index沒用到可以去掉
        result = result * base_num
    end

    return result
end

puts pow(5,7)