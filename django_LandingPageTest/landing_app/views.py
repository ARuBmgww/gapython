from django.shortcuts import render
from django.http import HttpResponse
from landing_app.models import Topic, Webpage, AccessRecord
# Create your views here.

def index(request):
    webpages_list = AccessRecord.objects.order_by('date')
    date_dict = {'access_records':webpages_list}
    return render(request,'signup.htm',context=date_dict)


    # my_dict = {'insert_me':""}
    # return render(request,'signup.htm',context=my_dict)

def thank(request):
    my_dict2 = {'insert_back':"Hello I am from views.py!"}
    return render(request,'Thankyou.htm',context=my_dict2)